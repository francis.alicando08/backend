const { Order, User, Product } = require('../models/Order');

// Create Order
async function createOrder(req, res) {
  try {
    const { userId, productIds } = req.body;

    // Retrieve user and product details
    const user = await User.findById(userId);
    const products = await Product.find({ _id: { $in: productIds } });

    // Calculate total price
    const totalPrice = products.reduce((total, product) => total + product.price, 0);

    // Create the order
    const order = new Order({
      user: user._id,
      products: products.map(product => product._id),
      totalPrice,
      status: 'pending',
      // Set other order details as needed
    });

    // Save the order to the database
    await order.save();

    res.status(201).json({ message: 'Order created successfully', order });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
}

module.exports = {
  createOrder
};