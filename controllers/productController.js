const Product = require("../models/Product");



module.exports.addproduct = (data) =>{

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newProduct = new Product({
		name : data.product.name,
		description : data.product.description,
		price : data.product.price,
		category : data.product.category,
		quantity : data.product.quantity,
		image : data.product.image
	});

	return newProduct.save().then((product, error) => {
		if(error){
			return false;

		}else {
			return true;
		};
	})
};




module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};

module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	});
};

// Retrieving a specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	});
};



// Update a product
module.exports.updateProduct = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

		// Product not updated
		if(error){
			return false;

		// Product updated successfully
		} else {
			return true;
		}
	})

};


module.exports.archiveProduct = (reqParams, reqBody) => {
  // Specify the fields/properties of the document to be updated
  let updateActiveField  = {
    isActive: false
  };

  return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
    // Product not found
    if (!product) {
      return false;
    }
    // Product archived successfully
    return true;
  });
};