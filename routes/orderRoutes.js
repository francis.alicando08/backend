
const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require("../auth");
const Product = require("../routes/productRoutes");
const User = require("../routes/userRoutes");


// Create order
router.post("/createOrder", (req, res) => {
    userController.createOrder(req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;