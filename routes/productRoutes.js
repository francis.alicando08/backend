const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


// Route for creating a product
router.post("/", auth.verify, (req, res) => {
	
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		productController.addproduct(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

});


// Route for retrieving all the products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});
	

// Route for retrieving all the active products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving a specific product
// Creating a product using "/:parametername" creates a dynamic route, meaning the url changes depending on the information provided
router.get("/:productId", (req, res) => {
	// console.log(req.params.productId);

	// Since the product ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the product ID by accessing the request's "params" property which contains all the parameters provided via the url
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {

const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
			res.send(false);
		}

});


// S40 Activity
// Route to archiving a product
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the products from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
	
});





// Allows us to export the "router" object that will be accessed in our index.js file
module.exports = router;
